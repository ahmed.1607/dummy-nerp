package com.craft.nerp.customer.application;

import com.craft.nerp.customer.domain.Customer;
import com.craft.nerp.customer.domain.CustomerCreator;
import com.craft.nerp.customer.domain.CustomerId;
import com.craft.nerp.customer.domain.CustomerToCreate;
import com.craft.nerp.customer.domain.CustomersRepository;
import java.util.Optional;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomersApplicationService {

  private final CustomersRepository customers;
  private final CustomerCreator customerCreator;

  public CustomersApplicationService(CustomersRepository customers) {
    this.customers = customers;

    customerCreator = new CustomerCreator(customers);
  }

  @Transactional
  public Customer create(CustomerToCreate customerToCreate) {
    return customerCreator.create(customerToCreate);
  }

  @Transactional(readOnly = true)
  public Optional<Customer> get(CustomerId customer) {
    return customers.get(customer);
  }
}
