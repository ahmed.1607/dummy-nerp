package com.craft.nerp.customer.infrastructure.primary;

import com.craft.nerp.customer.application.CustomersApplicationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/customers")
@Tag(name = "Customers", description = "Customers management")
class CustomersResource {

  private final CustomersApplicationService customers;

  public CustomersResource(CustomersApplicationService customers) {
    this.customers = customers;
  }

  @PostMapping
  @Operation(summary = "Create a customer")
  public ResponseEntity<RestCustomer> createCustomer(@Validated @RequestBody RestCustomerToCreate customerToCreate) {
    return new ResponseEntity<>(RestCustomer.from(customers.create(customerToCreate.toDomain())), HttpStatus.CREATED);
  }
}
