package com.craft.nerp.customer.infrastructure.primary;

import com.craft.nerp.customer.domain.CustomerFirstname;
import com.craft.nerp.customer.domain.CustomerLastname;
import com.craft.nerp.customer.domain.CustomerName;
import com.craft.nerp.customer.domain.CustomerToCreate;
import com.craft.nerp.customer.infrastructure.primary.RestCustomerToCreate.RestCustomerToCreateBuilder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

@JsonDeserialize(builder = RestCustomerToCreateBuilder.class)
@Schema(name = "CustomerToCreate", description = "Information to create a customer")
class RestCustomerToCreate {

  private final String firstname;
  private final String lastname;
  private final RestAddress address;

  private RestCustomerToCreate(RestCustomerToCreateBuilder builder) {
    firstname = builder.firstname;
    lastname = builder.lastname;
    address = builder.address;
  }

  public CustomerToCreate toDomain() {
    return new CustomerToCreate(buildName(), address.toDomain());
  }

  private CustomerName buildName() {
    return new CustomerName(new CustomerFirstname(firstname), new CustomerLastname(lastname));
  }

  @NotNull
  @Schema(description = "Firstname of this customer", requiredMode = RequiredMode.REQUIRED)
  public String getFirstname() {
    return firstname;
  }

  @NotNull
  @Schema(description = "Lastname of this customer", requiredMode = RequiredMode.REQUIRED)
  public String getLastname() {
    return lastname;
  }

  @Valid
  @NotNull
  @Schema(description = "Address of this customer", requiredMode = RequiredMode.REQUIRED)
  public RestAddress getAddress() {
    return address;
  }

  @JsonPOJOBuilder(withPrefix = "")
  static class RestCustomerToCreateBuilder {

    private String firstname;
    private String lastname;
    private RestAddress address;

    RestCustomerToCreateBuilder firstname(String firstname) {
      this.firstname = firstname;

      return this;
    }

    RestCustomerToCreateBuilder lastname(String lastname) {
      this.lastname = lastname;

      return this;
    }

    RestCustomerToCreateBuilder address(RestAddress address) {
      this.address = address;

      return this;
    }

    RestCustomerToCreate build() {
      return new RestCustomerToCreate(this);
    }
  }
}
