package com.craft.nerp.customer.infrastructure.secondary;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface JpaCustomersRepository extends JpaRepository<CustomerEntity, UUID> {}
