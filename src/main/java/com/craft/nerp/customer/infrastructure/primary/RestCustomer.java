package com.craft.nerp.customer.infrastructure.primary;

import com.craft.nerp.customer.domain.Customer;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import java.util.UUID;

@Schema(name = "Customer", description = "Information for a customer")
class RestCustomer {

  private final UUID id;
  private final String firstname;
  private final String lastname;
  private final RestAddress address;

  private RestCustomer(RestCustomerBuilder builder) {
    id = builder.id;
    firstname = builder.firstname;
    lastname = builder.lastname;
    address = builder.address;
  }

  public static RestCustomer from(Customer customer) {
    return new RestCustomerBuilder()
      .id(customer.id().value())
      .firstname(customer.firstname().value())
      .lastname(customer.lastname().value())
      .address(RestAddress.from(customer.address()))
      .build();
  }

  @Schema(description = "ID of this customer", requiredMode = RequiredMode.REQUIRED)
  public UUID getId() {
    return id;
  }

  @Schema(description = "Firstname of this customer", requiredMode = RequiredMode.REQUIRED)
  public String getFirstname() {
    return firstname;
  }

  @Schema(description = "Lastname of this customer", requiredMode = RequiredMode.REQUIRED)
  public String getLastname() {
    return lastname;
  }

  @Schema(description = "Address of this customer", requiredMode = RequiredMode.REQUIRED)
  public RestAddress getAddress() {
    return address;
  }

  private static class RestCustomerBuilder {

    private UUID id;
    private String firstname;
    private String lastname;
    private RestAddress address;

    public RestCustomerBuilder id(UUID id) {
      this.id = id;

      return this;
    }

    public RestCustomerBuilder firstname(String firstname) {
      this.firstname = firstname;

      return this;
    }

    public RestCustomerBuilder lastname(String lastname) {
      this.lastname = lastname;

      return this;
    }

    public RestCustomerBuilder address(RestAddress address) {
      this.address = address;

      return this;
    }

    public RestCustomer build() {
      return new RestCustomer(this);
    }
  }
}
