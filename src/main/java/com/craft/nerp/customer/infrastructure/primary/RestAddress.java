package com.craft.nerp.customer.infrastructure.primary;

import com.craft.nerp.customer.domain.address.CustomerAddress;
import com.craft.nerp.customer.infrastructure.primary.RestAddress.RestAddressBuilder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import jakarta.validation.constraints.NotNull;

@Schema(name = "Address")
@JsonDeserialize(builder = RestAddressBuilder.class)
class RestAddress {

  private final String street;
  private final String postalCode;
  private final String city;

  private RestAddress(RestAddressBuilder builder) {
    street = builder.street;
    postalCode = builder.postalCode;
    city = builder.city;
  }

  public CustomerAddress toDomain() {
    return CustomerAddress.builder().street(street).postalCode(postalCode).city(city);
  }

  public static RestAddress from(CustomerAddress address) {
    return new RestAddressBuilder()
      .street(address.street().value())
      .postalCode(address.postalCode().value())
      .city(address.city().value())
      .build();
  }

  @NotNull
  @Schema(description = "Street (with number) for this address", requiredMode = RequiredMode.REQUIRED)
  public String getStreet() {
    return street;
  }

  @NotNull
  @Schema(description = "Postal code for this address", requiredMode = RequiredMode.REQUIRED)
  public String getPostalCode() {
    return postalCode;
  }

  @NotNull
  @Schema(description = "City for this address", requiredMode = RequiredMode.REQUIRED)
  public String getCity() {
    return city;
  }

  @JsonPOJOBuilder(withPrefix = "")
  static class RestAddressBuilder {

    private String street;
    private String postalCode;
    private String city;

    RestAddressBuilder street(String street) {
      this.street = street;

      return this;
    }

    RestAddressBuilder postalCode(String postalCode) {
      this.postalCode = postalCode;

      return this;
    }

    RestAddressBuilder city(String city) {
      this.city = city;

      return this;
    }

    RestAddress build() {
      return new RestAddress(this);
    }
  }
}
