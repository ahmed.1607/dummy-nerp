package com.craft.nerp.customer.domain.address;

public class CustomerAddress {

  private final CustomerStreet street;
  private final CustomerPostalCode postalCode;
  private final CustomerCity city;

  private CustomerAddress(AddressBuilder builder) {
    street = new CustomerStreet(builder.street);
    postalCode = new CustomerPostalCode(builder.postalCode);
    city = new CustomerCity(builder.city);
  }

  public static AddressStreetBuilder builder() {
    return new AddressBuilder();
  }

  public CustomerStreet street() {
    return street;
  }

  public CustomerPostalCode postalCode() {
    return postalCode;
  }

  public CustomerCity city() {
    return city;
  }

  private static class AddressBuilder implements AddressStreetBuilder, AddressPostalCodeBuilder, AddressCityBuilder {

    private String street;
    private String postalCode;
    private String city;

    @Override
    public AddressPostalCodeBuilder street(String street) {
      this.street = street;

      return this;
    }

    @Override
    public AddressCityBuilder postalCode(String postalCode) {
      this.postalCode = postalCode;

      return this;
    }

    @Override
    public CustomerAddress city(String city) {
      this.city = city;

      return new CustomerAddress(this);
    }
  }

  public interface AddressStreetBuilder {
    AddressPostalCodeBuilder street(String street);
  }

  public interface AddressPostalCodeBuilder {
    AddressCityBuilder postalCode(String postalCode);
  }

  public interface AddressCityBuilder {
    CustomerAddress city(String city);
  }
}
