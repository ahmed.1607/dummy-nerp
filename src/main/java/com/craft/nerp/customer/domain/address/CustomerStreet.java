package com.craft.nerp.customer.domain.address;

import com.craft.nerp.shared.error.domain.Assert;

public record CustomerStreet(String value) {
  public CustomerStreet {
    Assert.field("street", value).notBlank().maxLength(255);
  }
}
