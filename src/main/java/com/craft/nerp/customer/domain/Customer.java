package com.craft.nerp.customer.domain;

import com.craft.nerp.customer.domain.address.CustomerAddress;
import com.craft.nerp.shared.error.domain.Assert;

public class Customer {

  private final CustomerId id;
  private final CustomerName name;
  private final CustomerAddress address;

  private Customer(CustomerBuilder builder) {
    Assert.notNull("id", builder.id);
    Assert.notNull("name", builder.name);
    Assert.notNull("address", builder.address);

    id = builder.id;
    name = builder.name;
    address = builder.address;
  }

  public static CustomerIdBuilder builder() {
    return new CustomerBuilder();
  }

  public CustomerId id() {
    return id;
  }

  public CustomerFirstname firstname() {
    return name.firstname();
  }

  public CustomerLastname lastname() {
    return name.lastname();
  }

  public CustomerAddress address() {
    return address;
  }

  private static class CustomerBuilder implements CustomerIdBuilder, CustomerNameBuilder, CustomerAddressBuilder {

    private CustomerId id;
    private CustomerName name;
    private CustomerAddress address;

    @Override
    public CustomerNameBuilder id(CustomerId id) {
      this.id = id;

      return this;
    }

    @Override
    public CustomerAddressBuilder name(CustomerName name) {
      this.name = name;

      return this;
    }

    @Override
    public Customer address(CustomerAddress address) {
      this.address = address;

      return new Customer(this);
    }
  }

  public interface CustomerIdBuilder {
    CustomerNameBuilder id(CustomerId id);
  }

  public interface CustomerNameBuilder {
    CustomerAddressBuilder name(CustomerName name);
  }

  public interface CustomerAddressBuilder {
    Customer address(CustomerAddress address);
  }
}
