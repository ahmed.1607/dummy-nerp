package com.craft.nerp.shared.customer_identity.domain;

import com.craft.nerp.shared.error.domain.Assert;

public class CustomerIdentity {

  private final String firstname;
  private final String lastname;
  private final String street;
  private final String postalCode;
  private final String city;

  private CustomerIdentity(CustomerIdentityBuilder builder) {
    assertMandatoryFields(builder);

    firstname = builder.firstname;
    lastname = builder.lastname;
    street = builder.street;
    postalCode = builder.postalCode;
    city = builder.city;
  }

  private void assertMandatoryFields(CustomerIdentityBuilder builder) {
    Assert.notBlank("firtname", builder.firstname);
    Assert.notBlank("lastname", builder.lastname);
    Assert.notBlank("street", builder.street);
    Assert.notBlank("postalCode", builder.postalCode);
    Assert.notBlank("city", builder.city);
  }

  public static CustomerIdentityFirstnameBuilder builder() {
    return new CustomerIdentityBuilder();
  }

  public String firstname() {
    return firstname;
  }

  public String lastname() {
    return lastname;
  }

  public String street() {
    return street;
  }

  public String postalCode() {
    return postalCode;
  }

  public String city() {
    return city;
  }

  private static class CustomerIdentityBuilder
    implements
      CustomerIdentityFirstnameBuilder,
      CustomerIdentityLastnameBuilder,
      CustomerIdentityStreetBuilder,
      CustomerIdentityPostalCodeBuilder,
      CustomerIdentityCityBuilder {

    private String firstname;
    private String lastname;
    private String street;
    private String postalCode;
    private String city;

    @Override
    public CustomerIdentityLastnameBuilder firstname(String firstname) {
      this.firstname = firstname;

      return this;
    }

    @Override
    public CustomerIdentityStreetBuilder lastname(String lastname) {
      this.lastname = lastname;

      return this;
    }

    @Override
    public CustomerIdentityPostalCodeBuilder street(String street) {
      this.street = street;

      return this;
    }

    @Override
    public CustomerIdentityCityBuilder postalCode(String postalCode) {
      this.postalCode = postalCode;

      return this;
    }

    @Override
    public CustomerIdentity city(String city) {
      this.city = city;

      return new CustomerIdentity(this);
    }
  }

  public interface CustomerIdentityFirstnameBuilder {
    CustomerIdentityLastnameBuilder firstname(String firstname);
  }

  public interface CustomerIdentityLastnameBuilder {
    CustomerIdentityStreetBuilder lastname(String lastname);
  }

  public interface CustomerIdentityStreetBuilder {
    CustomerIdentityPostalCodeBuilder street(String street);
  }

  public interface CustomerIdentityPostalCodeBuilder {
    CustomerIdentityCityBuilder postalCode(String postalCode);
  }

  public interface CustomerIdentityCityBuilder {
    CustomerIdentity city(String city);
  }
}
