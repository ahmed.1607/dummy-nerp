package com.craft.nerp.invoice.application;

import com.craft.nerp.invoice.domain.Invoice;
import com.craft.nerp.invoice.domain.InvoiceId;
import com.craft.nerp.invoice.domain.InvoiceToCreate;
import com.craft.nerp.invoice.domain.InvoicesCreator;
import com.craft.nerp.invoice.domain.InvoicesRepository;
import com.craft.nerp.invoice.domain.recipient.RecipientsRepository;
import java.util.Optional;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class InvoicesApplicationService {

  private final InvoicesRepository invoices;
  private final InvoicesCreator invoicesCreator;

  public InvoicesApplicationService(RecipientsRepository recipients, InvoicesRepository invoices) {
    this.invoices = invoices;
    invoicesCreator = new InvoicesCreator(recipients, invoices);
  }

  @Transactional(readOnly = true)
  public Optional<Invoice> get(InvoiceId invoiceId) {
    return invoices.get(invoiceId);
  }

  @Transactional
  public Invoice create(InvoiceToCreate invoiceToCreate) {
    return invoicesCreator.create(invoiceToCreate);
  }
}
