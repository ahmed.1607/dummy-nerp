package com.craft.nerp.invoice.domain.recipient.address;

import com.craft.nerp.shared.error.domain.Assert;

public class RecipientAddress {

  private final RecipientStreet street;
  private final RecipientPostalCode postalCode;
  private final RecipientCity city;

  private RecipientAddress(AddressBuilder builder) {
    Assert.notNull("street", builder.street);
    Assert.notNull("postalCode", builder.postalCode);
    Assert.notNull("city", builder.city);

    street = builder.street;
    postalCode = builder.postalCode;
    city = builder.city;
  }

  public static AddressStreetBuilder builder() {
    return new AddressBuilder();
  }

  public RecipientStreet street() {
    return street;
  }

  public RecipientPostalCode postalCode() {
    return postalCode;
  }

  public RecipientCity city() {
    return city;
  }

  private static class AddressBuilder implements AddressStreetBuilder, AddressPostalCodeBuilder, AddressCityBuilder {

    private RecipientStreet street;
    private RecipientPostalCode postalCode;
    private RecipientCity city;

    @Override
    public AddressPostalCodeBuilder street(RecipientStreet street) {
      this.street = street;

      return this;
    }

    @Override
    public AddressCityBuilder postalCode(RecipientPostalCode postalCode) {
      this.postalCode = postalCode;

      return this;
    }

    @Override
    public RecipientAddress city(RecipientCity city) {
      this.city = city;

      return new RecipientAddress(this);
    }
  }

  public interface AddressStreetBuilder {
    AddressPostalCodeBuilder street(RecipientStreet street);
  }

  public interface AddressPostalCodeBuilder {
    AddressCityBuilder postalCode(RecipientPostalCode postalCode);
  }

  public interface AddressCityBuilder {
    RecipientAddress city(RecipientCity city);
  }
}
