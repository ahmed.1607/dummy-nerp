package com.craft.nerp.invoice.domain;

import java.math.BigDecimal;

public class Line {

  private final LineLabel label;
  private final Quantity quantity;
  private final Amount unitPrice;

  private Line(LineBuilder builder) {
    label = new LineLabel(builder.label);
    quantity = new Quantity(builder.quantity);
    unitPrice = new Amount(builder.unitPrice);
  }

  public static LineLabelBuilder builder() {
    return new LineBuilder();
  }

  public LineLabel label() {
    return label;
  }

  public Quantity quantity() {
    return quantity;
  }

  public Amount unitPrice() {
    return unitPrice;
  }

  public Amount total() {
    return unitPrice.times(quantity);
  }

  private static class LineBuilder implements LineLabelBuilder, LineQuantityBuilder, LineUnitPriceBuilder {

    private String label;
    private int quantity;
    private BigDecimal unitPrice;

    @Override
    public LineQuantityBuilder label(String label) {
      this.label = label;

      return this;
    }

    @Override
    public LineUnitPriceBuilder quantity(int quantity) {
      this.quantity = quantity;

      return this;
    }

    @Override
    public Line unitPrice(BigDecimal unitPrice) {
      this.unitPrice = unitPrice;

      return new Line(this);
    }
  }

  public interface LineLabelBuilder {
    LineQuantityBuilder label(String label);
  }

  public interface LineQuantityBuilder {
    LineUnitPriceBuilder quantity(int quantity);
  }

  public interface LineUnitPriceBuilder {
    Line unitPrice(BigDecimal unitPrice);
  }
}
