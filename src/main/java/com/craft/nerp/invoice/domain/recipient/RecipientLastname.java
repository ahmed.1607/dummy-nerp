package com.craft.nerp.invoice.domain.recipient;

import com.craft.nerp.shared.error.domain.Assert;

public record RecipientLastname(String value) {
  public RecipientLastname {
    Assert.field("lastname", value).notBlank().maxLength(255);
  }
}
