package com.craft.nerp.invoice.domain;

import com.craft.nerp.invoice.domain.recipient.Recipient;
import com.craft.nerp.invoice.domain.recipient.RecipientsRepository;
import com.craft.nerp.shared.error.domain.Assert;

public class InvoicesCreator {

  private final RecipientsRepository recipients;
  private final InvoicesRepository invoices;

  public InvoicesCreator(RecipientsRepository recipients, InvoicesRepository invoices) {
    Assert.notNull("recipients", recipients);
    Assert.notNull("invoices", invoices);

    this.recipients = recipients;
    this.invoices = invoices;
  }

  public Invoice create(InvoiceToCreate invoiceToCreate) {
    Assert.notNull("invoiceToCreate", invoiceToCreate);

    Invoice invoice = invoiceToCreate.create(getRecipient(invoiceToCreate));
    invoices.save(invoice);

    return invoice;
  }

  private Recipient getRecipient(InvoiceToCreate invoiceToCreate) {
    return recipients.get(invoiceToCreate.recipient()).orElseThrow(() -> new UnknownRecipientException(invoiceToCreate.recipient()));
  }
}
