package com.craft.nerp.invoice.domain.recipient.address;

import com.craft.nerp.shared.error.domain.Assert;

public record RecipientPostalCode(String value) {
  public RecipientPostalCode {
    Assert.field("postalCode", value).notBlank().maxLength(255);
  }
}
