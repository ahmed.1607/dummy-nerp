package com.craft.nerp.invoice.domain.recipient.address;

import com.craft.nerp.shared.error.domain.Assert;

public record RecipientCity(String value) {
  public RecipientCity {
    Assert.field("city", value).notBlank().maxLength(255);
  }
}
