package com.craft.nerp.invoice.domain;

import com.craft.nerp.shared.error.domain.Assert;
import java.util.UUID;

public record InvoiceId(UUID value) {
  public InvoiceId {
    Assert.notNull("invoiceId", value);
  }

  public static InvoiceId newId() {
    return new InvoiceId(UUID.randomUUID());
  }
}
