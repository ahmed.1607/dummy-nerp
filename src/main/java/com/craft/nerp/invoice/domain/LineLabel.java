package com.craft.nerp.invoice.domain;

import com.craft.nerp.shared.error.domain.Assert;

public record LineLabel(String value) {
  public LineLabel {
    Assert.notBlank("lineLabel", value);
  }
}
