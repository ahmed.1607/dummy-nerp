package com.craft.nerp.invoice.domain.recipient;

import com.craft.nerp.shared.error.domain.Assert;
import java.util.UUID;

public record RecipientId(UUID value) {
  public RecipientId {
    Assert.notNull("recipientId", value);
  }

  @Override
  public String toString() {
    return value.toString();
  }
}
