package com.craft.nerp.invoice.domain.recipient;

import com.craft.nerp.shared.error.domain.Assert;

public record RecipientFirstname(String value) {
  public RecipientFirstname {
    Assert.field("firstname", value).notBlank().maxLength(255);
  }
}
