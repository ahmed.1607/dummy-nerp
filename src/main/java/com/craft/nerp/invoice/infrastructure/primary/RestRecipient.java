package com.craft.nerp.invoice.infrastructure.primary;

import com.craft.nerp.invoice.domain.recipient.Recipient;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;

@Schema(name = "Recipient", description = "Recipient of an invoice")
class RestRecipient {

  private final String firstname;
  private final String lastname;
  private final RestAddress address;

  private RestRecipient(RestRecipientBuilder builder) {
    firstname = builder.firstname;
    lastname = builder.lastname;
    address = builder.address;
  }

  public static RestRecipient from(Recipient recipient) {
    return new RestRecipientBuilder()
      .firstname(recipient.firstname().value())
      .lastname(recipient.lastname().value())
      .address(RestAddress.from(recipient.address()))
      .build();
  }

  @Schema(description = "Firstname of this recipient", requiredMode = RequiredMode.REQUIRED)
  public String getFirstname() {
    return firstname;
  }

  @Schema(description = "Lastname of this recipient", requiredMode = RequiredMode.REQUIRED)
  public String getLastname() {
    return lastname;
  }

  @Schema(description = "Address of this recipient", requiredMode = RequiredMode.REQUIRED)
  public RestAddress getAddress() {
    return address;
  }

  private static class RestRecipientBuilder {

    private String firstname;
    private String lastname;
    private RestAddress address;

    public RestRecipientBuilder firstname(String firstname) {
      this.firstname = firstname;

      return this;
    }

    public RestRecipientBuilder lastname(String lastname) {
      this.lastname = lastname;

      return this;
    }

    public RestRecipientBuilder address(RestAddress address) {
      this.address = address;

      return this;
    }

    public RestRecipient build() {
      return new RestRecipient(this);
    }
  }
}
