package com.craft.nerp.invoice.infrastructure.primary;

import com.craft.nerp.invoice.domain.recipient.address.RecipientAddress;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;

@Schema(name = "Address", description = "Address of a recipient")
class RestAddress {

  private final String street;
  private final String postalCode;
  private final String city;

  private RestAddress(RestAddressBuilder builder) {
    street = builder.street;
    postalCode = builder.postalCode;
    city = builder.city;
  }

  public static RestAddress from(RecipientAddress address) {
    return new RestAddressBuilder()
      .street(address.street().value())
      .postalCode(address.postalCode().value())
      .city(address.city().value())
      .build();
  }

  @Schema(description = "Street with number", requiredMode = RequiredMode.REQUIRED)
  public String getStreet() {
    return street;
  }

  @Schema(requiredMode = RequiredMode.REQUIRED)
  public String getPostalCode() {
    return postalCode;
  }

  @Schema(requiredMode = RequiredMode.REQUIRED)
  public String getCity() {
    return city;
  }

  private static class RestAddressBuilder {

    private String street;
    private String postalCode;
    private String city;

    public RestAddressBuilder street(String street) {
      this.street = street;

      return this;
    }

    public RestAddressBuilder postalCode(String postalCode) {
      this.postalCode = postalCode;

      return this;
    }

    public RestAddressBuilder city(String city) {
      this.city = city;

      return this;
    }

    public RestAddress build() {
      return new RestAddress(this);
    }
  }
}
