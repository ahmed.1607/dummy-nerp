package com.craft.nerp.invoice.infrastructure.secondary;

import com.craft.nerp.invoice.domain.Invoice;
import com.craft.nerp.invoice.domain.InvoiceId;
import com.craft.nerp.invoice.domain.Lines;
import com.craft.nerp.invoice.domain.recipient.Recipient;
import com.craft.nerp.invoice.domain.recipient.RecipientFirstname;
import com.craft.nerp.invoice.domain.recipient.RecipientLastname;
import com.craft.nerp.invoice.domain.recipient.RecipientName;
import com.craft.nerp.invoice.domain.recipient.address.RecipientAddress;
import com.craft.nerp.invoice.domain.recipient.address.RecipientCity;
import com.craft.nerp.invoice.domain.recipient.address.RecipientPostalCode;
import com.craft.nerp.invoice.domain.recipient.address.RecipientStreet;
import com.craft.nerp.shared.error.domain.NerpException;
import com.craft.nerp.shared.generation.domain.ExcludeFromGeneratedCodeCoverage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.UUID;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
@Table(name = "invoice")
class InvoiceEntity {

  @Id
  @Column(name = "id")
  private UUID id;

  @Column(name = "recipient_firstname")
  private String recipientFirstname;

  @Column(name = "recipient_lastname")
  private String recipientLastname;

  @Column(name = "recipient_street")
  private String recipientStreet;

  @Column(name = "recipient_postal_code")
  private String recipientPostalCode;

  @Column(name = "recipient_city")
  private String recipientCity;

  @Column(name = "lines")
  private String lines;

  public static InvoiceEntity from(ObjectMapper json, Invoice invoice) {
    Recipient recipient = invoice.recipient();
    RecipientAddress address = recipient.address();

    return new InvoiceEntity()
      .id(invoice.id().value())
      .recipientFirstname(recipient.firstname().value())
      .recipientLastname(recipient.lastname().value())
      .recipientStreet(address.street().value())
      .recipientPostalCode(address.postalCode().value())
      .recipientCity(address.city().value())
      .lines(serializeLines(json, invoice.lines()));
  }

  private InvoiceEntity id(UUID id) {
    this.id = id;

    return this;
  }

  private InvoiceEntity recipientFirstname(String recipientFirstname) {
    this.recipientFirstname = recipientFirstname;

    return this;
  }

  private InvoiceEntity recipientLastname(String recipientLastname) {
    this.recipientLastname = recipientLastname;

    return this;
  }

  private InvoiceEntity recipientStreet(String recipientStreet) {
    this.recipientStreet = recipientStreet;

    return this;
  }

  private InvoiceEntity recipientPostalCode(String recipientPostalCode) {
    this.recipientPostalCode = recipientPostalCode;

    return this;
  }

  private InvoiceEntity recipientCity(String recipientCity) {
    this.recipientCity = recipientCity;

    return this;
  }

  private InvoiceEntity lines(String lines) {
    this.lines = lines;

    return this;
  }

  private static String serializeLines(ObjectMapper json, Lines lines) {
    try {
      return json.writeValueAsString(SerializableLines.from(lines));
    } catch (JsonProcessingException e) {
      throw NerpException.technicalError("Error serializing lines: " + e.getMessage(), e);
    }
  }

  public Invoice toDomain(ObjectMapper json) {
    return Invoice.builder().id(new InvoiceId(id)).recipient(buildRecipient()).lines(deserializeLines(json));
  }

  private Recipient buildRecipient() {
    return new Recipient(buildName(), buildAddress());
  }

  private RecipientName buildName() {
    return new RecipientName(new RecipientFirstname(recipientFirstname), new RecipientLastname(recipientLastname));
  }

  private RecipientAddress buildAddress() {
    return RecipientAddress
      .builder()
      .street(new RecipientStreet(recipientStreet))
      .postalCode(new RecipientPostalCode(recipientPostalCode))
      .city(new RecipientCity(recipientCity));
  }

  private Lines deserializeLines(ObjectMapper json) {
    try {
      return json.readValue(lines, SerializableLines.class).toDomain();
    } catch (JsonProcessingException e) {
      throw NerpException.technicalError("Error deserializing lines:" + e.getMessage(), e);
    }
  }

  @Override
  @ExcludeFromGeneratedCodeCoverage
  public int hashCode() {
    return new HashCodeBuilder().append(id).hashCode();
  }

  @Override
  @ExcludeFromGeneratedCodeCoverage
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }

    InvoiceEntity other = (InvoiceEntity) obj;
    return new EqualsBuilder().append(id, other.id).isEquals();
  }
}
