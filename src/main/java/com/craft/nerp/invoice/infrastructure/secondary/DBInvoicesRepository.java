package com.craft.nerp.invoice.infrastructure.secondary;

import com.craft.nerp.invoice.domain.Invoice;
import com.craft.nerp.invoice.domain.InvoiceId;
import com.craft.nerp.invoice.domain.InvoicesRepository;
import com.craft.nerp.shared.error.domain.Assert;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Optional;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.stereotype.Repository;

@Repository
class DBInvoicesRepository implements InvoicesRepository {

  private final ObjectMapper json;
  private final JpaInvoicesRepository invoices;

  public DBInvoicesRepository(Jackson2ObjectMapperBuilder mapperBuilder, JpaInvoicesRepository invoices) {
    this.json = mapperBuilder.build();
    this.invoices = invoices;
  }

  @Override
  public Optional<Invoice> get(InvoiceId invoiceId) {
    Assert.notNull("invoiceId", invoiceId);

    return invoices.findById(invoiceId.value()).map(entity -> entity.toDomain(json));
  }

  @Override
  public void save(Invoice invoice) {
    Assert.notNull("invoice", invoice);

    invoices.save(InvoiceEntity.from(json, invoice));
  }
}
