package com.craft.nerp.invoice.infrastructure.primary;

import com.craft.nerp.invoice.application.InvoicesApplicationService;
import com.craft.nerp.invoice.domain.Invoice;
import com.craft.nerp.invoice.domain.InvoiceId;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.UUID;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/invoices")
@Tag(name = "Invoices", description = "Invoices management")
class InvoicesResource {

  private final InvoicesApplicationService invoices;

  public InvoicesResource(InvoicesApplicationService invoices) {
    this.invoices = invoices;
  }

  @PostMapping
  @Operation(summary = "Create an invoice")
  public ResponseEntity<RestInvoice> createInvoice(@Validated @RequestBody RestInvoiceToCreate invoiceToCreate) {
    Invoice invoice = invoices.create(invoiceToCreate.toDomain());

    return new ResponseEntity<>(RestInvoice.from(invoice), HttpStatus.CREATED);
  }

  @GetMapping("/{invoice-id}")
  @Operation(summary = "Get an invoice")
  public ResponseEntity<RestInvoice> getInvoice(@PathVariable("invoice-id") UUID invoiceId) {
    return ResponseEntity.of(invoices.get(new InvoiceId(invoiceId)).map(RestInvoice::from));
  }
}
