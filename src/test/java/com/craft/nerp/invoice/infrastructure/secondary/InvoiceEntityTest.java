package com.craft.nerp.invoice.infrastructure.secondary;

import static com.craft.nerp.JsonHelper.*;
import static com.craft.nerp.invoice.domain.InvoicesFixture.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.craft.nerp.UnitTest;
import com.craft.nerp.shared.error.domain.NerpException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.junit.jupiter.api.Test;

@UnitTest
class InvoiceEntityTest {

  @Test
  void shouldConvertFromAndToDomain() {
    assertThat(InvoiceEntity.from(jsonMapper(), invoice()).toDomain(jsonMapper())).usingRecursiveComparison().isEqualTo(invoice());
  }

  @Test
  void shouldHandleSerializationError() throws JsonProcessingException {
    ObjectMapper faillingObjectMapper = mock(ObjectMapper.class);
    doThrow(JsonProcessingException.class).when(faillingObjectMapper).writeValueAsString(any());

    assertThatThrownBy(() -> InvoiceEntity.from(faillingObjectMapper, invoice())).isExactlyInstanceOf(NerpException.class);
  }

  @Test
  void shouldHandleDeserializationError() throws IOException {
    ObjectMapper faillingObjectMapper = mock(ObjectMapper.class);
    doThrow(JsonProcessingException.class).when(faillingObjectMapper).readValue(any(String.class), eq(SerializableLines.class));

    assertThatThrownBy(() -> InvoiceEntity.from(jsonMapper(), invoice()).toDomain(faillingObjectMapper))
      .isExactlyInstanceOf(NerpException.class);
  }
}
