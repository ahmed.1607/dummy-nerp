package com.craft.nerp.invoice.infrastructure.primary;

import static com.craft.nerp.BeanValidationAssertions.*;
import static org.assertj.core.api.Assertions.*;

import com.craft.nerp.JsonHelper;
import com.craft.nerp.UnitTest;
import com.craft.nerp.invoice.domain.InvoicesFixture;
import com.craft.nerp.invoice.infrastructure.primary.RestLine.RestLineBuilder;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Test;

@UnitTest
class RestInvoiceToCreateTest {

  private static final String JSON =
    """
    {
      "recipient": "1e67ce12-a036-4362-a000-a926ea649ae2",
      "lines": [
        {
          "label": "First item",
          "quantity": 1,
          "unitPrice": 550
        },
        {
          "label": "Second item",
          "quantity": 2,
          "unitPrice": 1100
        }
      ]
    }
    """;

  @Test
  void shouldDeserializeFromJson() {
    RestInvoiceToCreate restInvoiceToCreate = JsonHelper.readFromJson(JSON, RestInvoiceToCreate.class);

    assertThat(restInvoiceToCreate.toDomain()).usingRecursiveComparison().isEqualTo(InvoicesFixture.invoiceToCreate());
  }

  @Test
  void shouldNotValidateWithoutRecipient() {
    assertThatBean(new RestInvoiceToCreate(null, null)).hasInvalidProperty("recipient");
  }

  @Test
  void shouldNotValidateWithoutLines() {
    assertThatBean(new RestInvoiceToCreate(recipient(), null)).hasInvalidProperty("lines");
  }

  @Test
  void shouldNotValidateWithEmptyLines() {
    assertThatBean(new RestInvoiceToCreate(recipient(), List.of())).hasInvalidProperty("lines");
  }

  @Test
  void shouldNotValidateWithInvalidLines() {
    assertThatBean(new RestInvoiceToCreate(recipient(), List.of(new RestLineBuilder().build()))).hasInvalidProperty("lines[0].quantity");
  }

  private static UUID recipient() {
    return UUID.fromString("1e67ce12-a036-4362-a000-a926ea649ae2");
  }
}
