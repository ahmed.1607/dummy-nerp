package com.craft.nerp.invoice.infrastructure.primary;

import static com.craft.nerp.invoice.domain.InvoicesFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.craft.nerp.JsonHelper;
import com.craft.nerp.UnitTest;
import org.junit.jupiter.api.Test;

@UnitTest
class RestInvoiceTest {

  @Test
  void shouldSerializeToJson() {
    String serialized = JsonHelper.writeAsString(RestInvoice.from(invoice()));

    assertThat(serialized).isEqualTo(json());
  }

  private String json() {
    return """
    {\
    "id":"8debd431-c12a-47c3-9532-7899da268b72",\
    "recipient":\
    {\
    "firstname":"Colin",\
    "lastname":"DAMON",\
    "address":{"street":"99 Rue d'ici","postalCode":"69000","city":"Lyon"}\
    },\
    "lines":[\
    {\
    "label":"First item",\
    "quantity":1,\
    "unitPrice":550.00\
    },\
    {\
    "label":"Second item",\
    "quantity":2,\
    "unitPrice":1100.00\
    }\
    ],\
    "total":2750.00\
    }\
    """;
  }
}
