package com.craft.nerp.invoice.domain.recipient;

import static org.assertj.core.api.Assertions.*;

import com.craft.nerp.UnitTest;
import com.craft.nerp.invoice.domain.InvoicesFixture;
import org.junit.jupiter.api.Test;

@UnitTest
class RecipientIdTest {

  @Test
  void shouldGetIdAsToString() {
    assertThat(InvoicesFixture.recipientId()).hasToString("1e67ce12-a036-4362-a000-a926ea649ae2");
  }
}
