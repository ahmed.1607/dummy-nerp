package com.craft.nerp.invoice.domain;

import static org.assertj.core.api.Assertions.*;

import com.craft.nerp.UnitTest;
import org.junit.jupiter.api.Test;

@UnitTest
class InvoiceIdTest {

  @Test
  void shouldGenerateId() {
    assertThat(InvoiceId.newId()).isNotNull().isNotEqualTo(InvoiceId.newId());
  }
}
