package com.craft.nerp.invoice.domain;

import com.craft.nerp.invoice.domain.recipient.Recipient;
import com.craft.nerp.invoice.domain.recipient.RecipientFirstname;
import com.craft.nerp.invoice.domain.recipient.RecipientId;
import com.craft.nerp.invoice.domain.recipient.RecipientLastname;
import com.craft.nerp.invoice.domain.recipient.RecipientName;
import com.craft.nerp.invoice.domain.recipient.address.RecipientAddress;
import com.craft.nerp.invoice.domain.recipient.address.RecipientCity;
import com.craft.nerp.invoice.domain.recipient.address.RecipientPostalCode;
import com.craft.nerp.invoice.domain.recipient.address.RecipientStreet;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public final class InvoicesFixture {

  private InvoicesFixture() {}

  public static Invoice invoice() {
    return Invoice.builder().id(invoiceId()).recipient(recipient()).lines(lines());
  }

  public static InvoiceToCreate invoiceToCreate() {
    return new InvoiceToCreate(recipientId(), lines());
  }

  public static RecipientId recipientId() {
    return new RecipientId(UUID.fromString("1e67ce12-a036-4362-a000-a926ea649ae2"));
  }

  public static InvoiceId invoiceId() {
    return new InvoiceId(UUID.fromString("8debd431-c12a-47c3-9532-7899da268b72"));
  }

  private static Recipient recipient() {
    return new Recipient(recipientName(), recipientAddress());
  }

  private static RecipientName recipientName() {
    return new RecipientName(recipientFirstname(), recipientLastname());
  }

  private static RecipientFirstname recipientFirstname() {
    return new RecipientFirstname("Colin");
  }

  private static RecipientLastname recipientLastname() {
    return new RecipientLastname("DAMON");
  }

  private static RecipientAddress recipientAddress() {
    return RecipientAddress.builder().street(recipientStreet()).postalCode(recipuientPostalCode()).city(recipientCity());
  }

  private static RecipientStreet recipientStreet() {
    return new RecipientStreet("99 Rue d'ici");
  }

  private static RecipientPostalCode recipuientPostalCode() {
    return new RecipientPostalCode("69000");
  }

  private static RecipientCity recipientCity() {
    return new RecipientCity("Lyon");
  }

  public static Lines lines() {
    return new Lines(List.of(firstLine(), secondLine()));
  }

  private static Line firstLine() {
    return Line.builder().label("First item").quantity(1).unitPrice(new BigDecimal("550"));
  }

  private static Line secondLine() {
    return Line.builder().label("Second item").quantity(2).unitPrice(new BigDecimal("1100"));
  }
}
