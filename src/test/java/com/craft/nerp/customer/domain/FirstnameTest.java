package com.craft.nerp.customer.domain;

import static org.assertj.core.api.Assertions.*;

import com.craft.nerp.UnitTest;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

@UnitTest
class FirstnameTest {

  @ParameterizedTest
  @MethodSource("firstnames")
  void shouldFormatFirstname(String raw, String formatted) {
    assertThat(new CustomerFirstname(raw).value()).isEqualTo(formatted);
  }

  static Stream<Arguments> firstnames() {
    return Stream.of(
      Arguments.of("colin", "Colin"),
      Arguments.of(" colin ", "Colin"),
      Arguments.of("jean-paul", "Jean-Paul"),
      Arguments.of("marie andrée", "Marie Andrée")
    );
  }
}
