package com.craft.nerp.customer.domain;

import com.craft.nerp.customer.domain.address.CustomerAddress;
import java.util.UUID;

public class CustomersFixture {

  private CustomersFixture() {}

  public static Customer customer() {
    return Customer.builder().id(customerId()).name(name()).address(address());
  }

  public static CustomerToCreate customerToCreate() {
    return new CustomerToCreate(name(), address());
  }

  public static CustomerId customerId() {
    return new CustomerId(UUID.fromString("1e67ce12-a036-4362-a000-a926ea649ae2"));
  }

  private static CustomerName name() {
    return new CustomerName(firstname(), lastname());
  }

  private static CustomerFirstname firstname() {
    return new CustomerFirstname("Colin");
  }

  private static CustomerLastname lastname() {
    return new CustomerLastname("DAMON");
  }

  private static CustomerAddress address() {
    return CustomerAddress.builder().street("99 Rue d'ici").postalCode("69000").city("Lyon");
  }
}
