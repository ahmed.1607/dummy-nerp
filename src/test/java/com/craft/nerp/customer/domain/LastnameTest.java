package com.craft.nerp.customer.domain;

import static org.assertj.core.api.Assertions.*;

import com.craft.nerp.UnitTest;
import org.junit.jupiter.api.Test;

@UnitTest
class LastnameTest {

  @Test
  void shouldFormatLastname() {
    assertThat(new CustomerLastname(" DéMon ").value()).isEqualTo("DEMON");
  }
}
