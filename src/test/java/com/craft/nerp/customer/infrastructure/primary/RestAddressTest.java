package com.craft.nerp.customer.infrastructure.primary;

import static com.craft.nerp.BeanValidationAssertions.*;

import com.craft.nerp.UnitTest;
import com.craft.nerp.customer.infrastructure.primary.RestAddress.RestAddressBuilder;
import org.junit.jupiter.api.Test;

@UnitTest
class RestAddressTest {

  @Test
  void shouldNotValidateEmptyAddress() {
    assertThatBean(new RestAddressBuilder().build())
      .hasInvalidProperty("street")
      .and()
      .hasInvalidProperty("postalCode")
      .and()
      .hasInvalidProperty("city");
  }
}
