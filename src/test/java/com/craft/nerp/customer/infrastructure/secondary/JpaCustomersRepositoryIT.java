package com.craft.nerp.customer.infrastructure.secondary;

import static com.craft.nerp.customer.domain.CustomersFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.craft.nerp.IntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@IntegrationTest
class JpaCustomersRepositoryIT {

  @Autowired
  private JpaCustomersRepository customers;

  @Test
  void shouldSaveAndGetCustomer() {
    customers.saveAndFlush(CustomerEntity.from(customer()));

    assertThat(customers.findById(customerId().value()).get().toDomain()).usingRecursiveComparison().isEqualTo(customer());
  }
}
