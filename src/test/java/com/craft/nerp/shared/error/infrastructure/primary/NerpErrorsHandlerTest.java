package com.craft.nerp.shared.error.infrastructure.primary;

import static org.mockito.Mockito.*;

import ch.qos.logback.classic.Level;
import com.craft.nerp.Logs;
import com.craft.nerp.LogsSpy;
import com.craft.nerp.LogsSpyExtension;
import com.craft.nerp.UnitTest;
import com.craft.nerp.shared.error.domain.NerpException;
import com.craft.nerp.shared.error.domain.StandardErrorKey;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.MessageSource;

@UnitTest
@ExtendWith(LogsSpyExtension.class)
class NerpErrorsHandlerTest {

  private static final NerpErrorsHandler handler = new NerpErrorsHandler(mock(MessageSource.class));

  @Logs
  private LogsSpy logs;

  @Test
  void shouldLogServerErrorAsError() {
    handler.handleNerpException(NerpException.internalServerError(StandardErrorKey.INTERNAL_SERVER_ERROR).message("Oops").build());

    logs.shouldHave(Level.ERROR, "Oops");
  }

  @Test
  void shouldLogClientErrorAsInfo() {
    handler.handleNerpException(NerpException.badRequest(StandardErrorKey.BAD_REQUEST).message("Oops").build());

    logs.shouldHave(Level.INFO, "Oops");
  }
}
