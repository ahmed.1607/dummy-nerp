package com.craft.nerp.shared.error_generator.infrastructure.primary;

import com.craft.nerp.shared.error.domain.NerpException;
import com.craft.nerp.shared.error.domain.StandardErrorKey;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/errors")
class NerpErrorsResource {

  @GetMapping("bad-request")
  void getBadRequest() {
    throw NerpException.badRequest(StandardErrorKey.BAD_REQUEST).addParameter("code", "400").build();
  }
}
